<?php
/**
 *	Let WordPress manage the document.title
 */
add_theme_support( 'title-tag' );

/**
 *	Enable support for Post Thumbnails on Posts and Pages
 */
add_theme_support( 'post-thumbnails' );

/**
 *	Enable support for menus
 */
add_theme_support( 'menus' );

/**
 *  Enable RSS Support
 */
add_theme_support( 'automatic-feed-links' );

/**
 * Disable Gutenberg block patterns.
 */
remove_theme_support( 'core-block-patterns' );

