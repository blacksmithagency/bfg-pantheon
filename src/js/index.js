import './utils/polyfills';

import Menu from './layout/Menu';

import Front from './pages/Front';

class App {
	constructor() {
		this.content = document.querySelector( '#main-content' );
		this.template = this.content.dataset.template;

		this.createMenu();

		this.createPages();
	}

	/**
	 * Load Component Classes
	 */
	createMenu() {
		this.menu = new Menu();
	}

	/**
	 * Load Page Classes
	 */
	createPages() {
		this.content = document.querySelector( '#main-content' );

		this.pages = new Map();

		this.pages.set( 'front', new Front() );

		this.page = this.pages.get( this.template ) || this.pages.get( 'front' );
		this.page.create();
	}
}

window.APP = new App();
