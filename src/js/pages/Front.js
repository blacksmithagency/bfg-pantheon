import Page from '../classes/Page';

export default class extends Page {
	constructor() {
		super({
			element: '#front-page',
			elements: {
				hero: '.front-hero',
				title: '.front-hero__title',
				body: '.front__body'
			}
		});
	}

	create() {
		super.create();
	}

	destroy() {
		super.destroy();
	}
}
