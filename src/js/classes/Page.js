import EventEmitter from 'events';

import each from 'lodash/each';

export default class extends EventEmitter {
	constructor({ element, elements }) {
		super();

		this.selectors = {
			element,
			elements
		};
	}

	// Select all elements from the DOM that are called on when the Class is instantiated.
	create() {
		this.element = document.querySelector( this.selectors.element );
		this.elements = {};

		each( this.selectors.elements, ( selector, key ) => {
			this.elements[key] = document.querySelector( selector );
		});
	}

	addEventListeners() {

	}

	removeEventListeners() {

	}

	destroy() {

	}
}
