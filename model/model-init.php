<?php

define( 'MODEL_DIR', TPL_PATH . '/model' );

include 'core/core-utils.php';
if( ! class_exists('ACF') ) :
    include 'core/core-acf.php';
endif;
include 'core/core-cpt.php';
include 'core/core-init.php';
