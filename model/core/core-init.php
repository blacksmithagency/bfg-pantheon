<?php

/**
 * core_load_acf_conf
 *
 * Loads configuration files in
 * the specified directory.
 *
 * @param string $confDir
 * @return NA
 */
function core_load_acf_conf( $confDir ) {
	$confList = scandir( $confDir );

	// Bail early if no custom field
	// configuration files are found.
	if ( ! $confList ) return;

	foreach ( $confList as $confFile ) {
		if (
			$confFile === '.' ||
			$confFile === '..'  ||
			strpos( $confFile, '.swp' )
		) continue;

		$confFilePath = "{$confDir}/{$confFile}";
		$confFileInfo = pathinfo( $confFilePath );

		if (
			isset( $confFileInfo['extension'] ) &&
			$confFileInfo['extension'] === 'php'
		) {
			include $confFilePath;
		} elseif ( ! isset( $confFileInfo['extension'] ) ) {
			core_load_acf_conf( $confFilePath );
		}
	}
}

/**
 * core_add_notice
 *
 * Adds a new notice to the current session.
 *
 * @param string $type="error"
 * @param string $message=""
 * @return NA
 */
function core_add_notice( $type="error", $message="" ) {
	if ( ! isset( $_SESSION['core_admin_notices'] ) ) {
		$_SESSION['core_admin_notices'] = [];
	}

	$_SESSION['core_admin_notices'][] = [
		'type' => $type,
		'message' => $message,
	];
}

/** Load: Content Types */
function core_load_content_types() {
	include_once MODEL_DIR . '/content/content-types.php';
}

add_action( 'init', 'core_load_content_types' );

/** Load: Custom Fields & Blocks */
function core_load_custom_fields_blocks() {
	/** Custom Fields */
	$cfDir = MODEL_DIR . '/custom-fields';
	core_load_acf_conf( $cfDir );

	/** Custom Blocks */
	$cbDir = MODEL_DIR . '/custom-blocks';
	core_load_acf_conf( $cbDir );
}

add_action( 'acf/init', 'core_load_custom_fields_blocks' );

/** Show admin notices. */
function core_show_admin_notices() {
	if ( isset( $_SESSION['core_admin_notices'] ) ) {
		foreach ( $_SESSION['core_admin_notices'] as $notice ): ?>
			<div class="notice notice-<?= $notice['type']; ?>">
				<p><?= $notice['message']; ?></p>
			</div>
		<?php endforeach;
	}
}

add_action( 'admin_notices', 'core_show_admin_notices' );

