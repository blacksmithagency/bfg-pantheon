<?php

/**
 * core_asset_url
 *
 * Return the formated static asset URL
 * from inside the theme directory.
 *
 * @param string $path
 * @return string
 */
function core_asset_url( string $path ) {
	return TPL_URL . "/src/assets/{$path}";
}

/**
 * core_is_assoc
 *
 * Checks if the specified array is
 * associative or sequential.
 *
 * @param array $arr
 * @return bool
 */
function core_is_assoc( $arr ) {
	if ( [] === $arr ) return false;
	return array_keys( $arr ) !== range( 0, count( $arr ) - 1 );
}

/**
 * core_get_all_image_sizes
 *
 * Retrieves an image with all of its
 * size variations.
 *
 * @param number $imageId=0
 * @return array
 */
function core_get_all_image_sizes( $imageId=0 ) {
	$images = [];
	foreach ( [ 'thumbnail', 'medium', 'large' ] as $size ) {
		$image = wp_get_attachment_image_src( $imageId, $size )[0];
		$images[ $size ] = $image ? $image : '';
	}

	return $images;
}

/**
 * core_format_post_object
 *
 * Retrieves and formats the
 * specified post data.
 *
 * @type function
 * @since 0.0.1
 *
 * @param int $postId
 * @return array
 */
function core_format_post_object( $postId ) {
	// Get post thumbnail and fields.
	$postThumb = core_get_all_image_sizes( get_post_thumbnail_id( $postId ) );
	if ( ! $postThumb ) $postThumb = [];
	$postFields = core_format_post_fields( get_fields( $postId ) );

	// Get post terms.
	$postTaxonomies = get_post_taxonomies( $postId );
	$postTerms = [];

	if ( ! empty( $postTaxonomies ) ) {
		foreach ( $postTaxonomies as $taxonomy ) {
			$postTerms[ $taxonomy ] = [];
			$taxonomyTerms = get_the_terms( $postId, $taxonomy );

			if ( ! empty( $taxonomyTerms ) ) {
				foreach ( $taxonomyTerms as $term ) {
					$postTerms[ $taxonomy ][] = [
						'ID' => $term->term_id,
						'title' => $term->name,
						'slug' => $term->slug,
					];
				}
			}
		}
	}

	// Get post dates.
	$publishDate = get_the_date( 'U', $postId );
	$modifiedDate = get_the_modified_date( 'U', $postId );

	// Prepare the post array.
	$formattedPost = [
		'ID' => $postId,
		'permalink' => get_permalink( $postId ),
		'date' => '' . $publishDate,
		'date_formatted' => date( 'F jS, Y', $publishDate ),
		'modified' => '' . $modifiedDate,
		'modified_formatted' => date( 'm/d/y', $modifiedDate ),
		'title' => get_the_title( $postId ),
		'featured_image' => $postThumb,
		'fields' => $postFields,
		'terms' => $postTerms,
	];

	return $formattedPost;
}

/**
 * core_get_posts
 *
 * Retrieves a list of posts from
 * the specified post type.
 *
 * @type function
 * @since 0.0.1
 *
 * @param string $postType
 * @param array $args
 * @param number $page=1
 * @return array
 */
function core_get_posts( $postType, $args, $page=1 ) {
	/** @var array */
	$defaultArgs = [
		'post_type' => $postType,
		'posts_per_page' => 10,
		'post_status' => 'publish',
		'paged' => $page,
		'fields' => 'ids',
	];

	foreach ( $defaultArgs as $key => $value ) {
		if ( ! isset( $args[ $key ] ) )
			$args[ $key ] = $value;
	}

	// Set up WP_Query.
	$query = new WP_Query( $args );

	// Format posts.
	$postsList = array_map( 'core_format_post_object', $query->posts );

	return [
		'results' => $postsList,
		'pagination' => [
			'page' => $page,
			'total' => $query->max_num_pages,
		],
	];
}

