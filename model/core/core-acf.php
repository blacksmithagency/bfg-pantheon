<?php

// Add options pages.
if ( function_exists( 'acf_add_options_sub_page' ) ) {
	/** @var array */
	$optionsPages = [
		/* 'global' => [ */
		/* 	'page_title' => 'Global', */
		/* 	'menu_title' => 'Global', */
		/* 	'capability' => 'edit_posts', */
		/* 	'child_pages' => [ */
		/* 		[ */
		/* 			'page_title' => '<++>', */
		/* 			'menu_title' => '<++>', */
		/* 			'menu_slug' => '<++>', */
		/* 		], */
		/* 	], */
		/* ], */
	];

	// Add options pages.
	foreach ( $optionsPages as $pageKey => $pageArgs ) {
		$pageArgs['menu_slug'] = $pageKey;

		$childPages = $pageArgs['child_pages'] ?? false;
		unset( $pageArgs['child_pages'] );

		acf_add_options_page( $pageArgs );

		// Add child pages.
		foreach ( $childPages as $childKey => $childArgs ) {
			$childArgs['parent_slug'] = $pageKey;
			acf_add_options_page( $childArgs );
		}
	}
}

/**
 * sanitize_title_underscore
 *
 * Sanitizes title strings, replacing
 * dashes with underscores.
 *
 * @param string $str=""
 * @return string
 */
function sanitize_title_underscore( $str="" ) {
	return str_replace( '-', '_', sanitize_title( $str ) );
}

/**
 * core_format_field
 *
 * Formats the arguments for a new Custom Field.
 *
 * @param string $type="text"
 * @param string $label=""
 * @param array $args=[]
 * @param string $uniqueKey=""
 * @return array
 */
function core_format_field( $type="text", $label="", $args=[], $uniqueKey="" ) {
	// Store the main field info.
	$args['type'] = $type;
	$args['label'] = $label;

	// Set the field name, if none is specified.
	if ( ! array_key_exists( 'name', $args ) ) {
		// Set the name prefix.
		//
		// For Tab and Accordion fields,
		// this is useful to avoid conflicts.
		$prefix = '';
		if ( in_array( $type, [ 'tab', 'accordion' ] ) ) {
			$prefix = "core_{$type}_{$uniqueKey}_";
		}

		$args['name'] = $prefix . sanitize_title_underscore( $label );
	}

	// Set the field key, if none is specified.
	if ( ! array_key_exists( 'key', $args ) ) {
		$args['key'] = join( '_', [
			$type === 'tab' ? 'core_field_tab' : 'core_field',
			$uniqueKey,
			$args['name'],
		]);
	}

	// Set up Flexible Content fields.
	if ( $type === 'flexible_content' && array_key_exists( 'layouts', $args ) ) {
		$layouts = [];

		foreach ( $args['layouts'] as $layout ) {
			$layoutKey = "core_layout_{$args['key']}_" . sanitize_title_underscore( $layout[0] );

			// Set layout args.
			$layouts[ $layoutKey ] = $layout[1];
			$layouts[ $layoutKey ]['name'] = sanitize_title_underscore( $layout[0] );
			$layouts[ $layoutKey ]['label'] = $layout[0];

			// Create the sub fields for the layout.
			if ( array_key_exists( 'sub_fields', $layout[1] ) ) {
				$layoutFields = [];

				foreach ( $layout[1]['sub_fields'] as $field ) {
					$layoutFields[] = core_format_field(
						$field[0],
						$field[1],
						$field[2],
						$layoutKey
					);
				}

				$layouts[ $layoutKey ]['sub_fields'] = $layoutField;
			}
		}

		$args['layouts'] = $layouts;
	}

	// Modify the keys to add the Repeater or Group key so
	// the field key doesn't end up duplicated somewhere.
	if (
		in_array( $type, [ 'repeater', 'group' ] ) &&
		array_key_exists( 'sub_fields', $args )
	) {
		foreach ( $args['sub_fields'] as $key => $subField ) {
			$newKey = "core_{$type}_field_{$args['key']}";
			$fieldArgs = count( $subField ) > 2 ? $subField[2] : [];
			$args['sub_fields'][ $key ] = core_format_field(
				$subField[0],
				$subField[1],
				$fieldArgs,
				$newKey
			);
		}
	}

	return $args;
}

/**
 * core_location_is
 *
 * Formats a Field Group's location.`
 *
 * @param array $locationIs
 * @return array
 */
function core_location_is( $locationIs=[] ) {
	if ( ! is_array( $locationIs ) ) return;

	$location[][] = [
		'param' => $locationIs[0],
		'operator' => '===',
		'value' => $locationIs[1],
	];

	return $location;
}

/**
 * core_register_field_group
 *
 * Registers a new Field Group.
 *
 * @param string $uniqueKey=""
 * @param array $groupArgs=[]
 * @param array $fields=[]
 * @return NA
 */
function core_register_field_group( $uniqueKey="", $groupArgs=[], $fields=[] ) {
	if ( ! isset( $groupArgs['title'] ) ) return;

	// Define the unique key.
	$uniqueKey = md5( $uniqueKey );

	// Set the group key.
	$groupArgs['key'] = "core_field_group_{$uniqueKey}_" . sanitize_title_underscore( $groupArgs['title'] );

	// Check for non-section fields.
	$nonSectionFields = array_filter( $fields, function( $field ) {
		return $field[0] !== 'section';
	});

	if ( ! empty( $nonSectionFields ) ) {
		core_add_notice(
			'error',
			'ACF configuration error. All fields must be wrapped within sections.'
		);
		return;
	}

	// Define the fields.
	foreach ( $fields as $field ) {
		$groupArgs['fields'][] = core_format_field( 'tab', $field[1], [
			'placement' => 'left',
		], $uniqueKey );

		$groupArgs['fields'][] = core_format_field( 'group', "{$field[1]} Section (Content)", [
			'name' => sanitize_title_underscore( $field[1] ),
			'sub_fields' => $field[2],
		], $uniqueKey );
	}

	acf_add_local_field_group( $groupArgs );
}

/**
 * core_format_post_fields
 *
 * Adds custom formatting to some post fields.
 *
 * @param array $fields
 * @param array $continueDown=true
 * 							Wether or not to continue formatting fields on the next level.
 * @return array
 */
function core_format_post_fields( $fields, $continueDown=true ) {
	if ( ! is_array( $fields ) ) return $fields;

	foreach ( $fields as $fieldKey => $fieldData ) {
		if (
			! isset( $fieldData['type'] ) ||
			! isset( $fieldData['value'] )
		) continue;
		$fieldType = $fieldData['type'];
		$fieldValue = $fieldData['value'];

		switch ( $fieldType ) {
			case 'relationship':
				if ( is_array( $fieldValue ) && ! empty( $fieldValue ) ) {
					foreach ( $fieldValue as $x => $post ) {
						$postThumb = core_get_all_image_sizes( get_post_thumbnail_id( $post->ID ) );
						$postFields = get_fields( $post->ID );
						if ( $continueDown ) $postFields = core_format_post_fields( $postFields, false );

						// Get post dates.
						$publishDate = get_the_date( 'U', $post->ID );
						$modifiedDate = get_the_modified_date( 'U', $post->ID );

						$fieldValue[ $x ] = [
							'ID' => $post->ID,
							'permalink' => get_permalink( $post->ID ),
							'date' => '' . $publishDate,
							'date_formatted' => date( 'F jS, Y', $publishDate ),
							'modified' => '' . $modifiedDate,
							'modified_formatted' => date( 'm/d/y', $modifiedDate ),
							'title' => $post->post_title,
							'featured_image' => $postThumb,
							'fields' => $postFields,
						];
					}
				}
				break;

			case 'group':
				$fieldValue = core_format_post_fields( $fieldValue );
				break;

			case 'repeater':
				if ( is_array( $fieldValue ) ) {
					$fieldValue = array_map( 'core_format_post_fields', $fieldValue );
				}
				break;
		}

		$fields[ $fieldKey ] = $fieldValue;
	}

	return $fields;
}

/**
 * core_get_field
 *
 * Returns the unformatted field value.
 *
 * @param string $fieldName
 * @param mixed $location=false
 * @return mixed
 */
function core_get_field( $fieldName, $location=false ) {
	if ( ! $location ) $location = get_the_id();

	$fieldData = get_field( $fieldName, $location );
	if ( isset( $fieldData['type'] ) )
		$fieldData = core_format_post_fields([ $fieldData ])[0];

	return $fieldData;
}

/**
 * core_timber_custom_options
 *
 * Stores the global options in the
 * Timber context.
 *
 * @param array $context
 * @return array
 */
function core_timber_custom_options( $context ) {
	$context['options'] = get_fields( 'option' );
	return $context;
}

add_filter( 'timber_context', 'core_timber_custom_options' );

/**
 * core_format_custom_fields
 *
 * Adds the field type to custom fields'
 * returning value.
 *
 * @type function
 * @since 0.0.1
 *
 * @param mixed $value
 * @param number $postId
 * @param array $field
 * @return array
 */
function core_format_custom_fields( $value, $postId, $field ) {
	$value = [
		'type' => $field['type'],
		'value' => $value,
	];

	return $value;
}

add_filter( 'acf/format_value', 'core_format_custom_fields', 10, 3 );
