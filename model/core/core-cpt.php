<?php

/**
 * core_default
 *
 * Sets a default value to an array,
 * if no value is set.
 *
 * @param string $key=""
 * @param array $args=[]
 * @param mixed $default=""
 * @return mixed
 */
function core_default( $key="", $args=[], $default="" ) {
	if ( ! array_key_exists( $key, $args ) ) return $default;
	return $args[ $key ];
}

/**
 * core_post_type
 *
 * Registers a Custom Post Type.
 *
 * @param string $label
 * @param array $args=[]
 * @return NA
 */
function core_post_type( $label, $args=[] ) {
	$args['label'] = $label;

	// Set defaults.
	$args['slug'] = core_default( 'slug', $args, sanitize_title( $label ) );
	$args['public'] = core_default( 'public', $args, true );

	register_post_type( $args['slug'], $args );
}

/**
 * core_taxonomy
 *
 * Registers a Custom Taxonomy.
 *
 * @param string $singularLabel
 * @param string $pluralLabel
 * @param string $postType
 * @param array $args=[]
 * @return NA
 */
function core_taxonomy( $singularLabel, $pluralLabel, $postType, $args=[] ) {
	// Set the labels.
	$args['label'] = $singular;
	$args['labels'] = [
		'name' => $pluralLabel,
		'singular_name' => $pluralLabel,
		'search_items' => "Search {$pluralLabel}",
		'popular_items' => NULL,
		'all_items' => $pluralLabel,
		'parent_item' => "Parent {$singularLabel}",
		'parent_item_colon' => "Parent {$singularLabel}:",
		'edit_item' => "Edit {$singularLabel}",
		'view_item' => "View {$singularLabel}",
		'update_item' => "Update {$singularLabel}",
		'add_new_item' => "Add New {$singularLabel}",
		'new_item_name' => "New {$singularLabel} Name",
		'separate_items_with_commas' => NULL,
		'add_or_remove_items' => NULL,
		'choose_from_most_used' => NULL,
		'not_found' => "No {$pluralLabel} found.",
		'no_terms' => "No {$pluralLabel}",
		'items_list_navigation' => "{$pluralLabel} list navigation",
		'items_list' => "{$pluralLabel} list",
		'most_used' => "Most Used",
		'back_to_items' => "← Back to {$pluralLabel}",
		'menu_name' => $pluralLabel,
		'name_admin_bar' => $pluralLabel,
		'archives' => $pluralLabel,
	];

	// Set defaults.
	$args['id'] = core_default( 'id', $args, true );
	$args['slug'] = core_default( 'slug', $args, sanitize_title( $singularLabel ) );
	$args['public'] = core_default( 'public', $args, true );
	$args['hierarchical'] = core_default( 'hierarchical', $args, true );
	$args['show_admin_column'] = core_default( 'show_admin_column', $args, true );

	// Register the Custom Taxonomy.
	register_taxonomy( $args['id'], $postType, $args );
}

