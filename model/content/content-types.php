<?php

// Post Types

/** Register Post Type: <++> */
/* core_post_type( '<++>', [ */
/* 	'slug' => '<++>', */
/* 	'supports' => [ 'title', 'thumbnail', 'editor' ], */
/* 	'hierarchical' => false, */
/* 	'menu_position' => 5, */
/* 	'menu_icon' => 'dashicons-<++>', */
/* 	'can_export' => false, */
/* 	'has_archive' => false, */
/* 	'capability_type' => 'page', */
/* 	'show_in_rest' => true, */
/* 	'rewrite' => [ */
/* 		'slug' => '<++>', */
/* 	], */
/* ]); */

// Taxonomies

/** Register Taxonomy: <++> */
/* core_taxonomy( '<++>', '<++>', [ '<++>' ], [ */
/* 	'slug' => '<++>', */
/* ]); */

